# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: AwesomeHaircut <jesusbalbastro at gmail com>
# Contributor: Mateusz Gozdek <mgozdekof@gmail.com>
# Contributor: Rein Fernhout <public@reinfernhout.xyz>
# Past Contributor: James An <james@jamesan.ca>

pkgname=droidcam
pkgver=1.8.0
pkgrel=1.1
pkgdesc='A tool for using your android device as a wireless/usb webcam'
arch=('x86_64')
url="https://github.com/dev47apps/${pkgname}"
license=('GPL')
makedepends=('gtk3' 'libappindicator-gtk3' 'ffmpeg' 'libusbmuxd' 'imagemagick')
options=('!strip')
install="${pkgname}.install"
source=("${pkgname}-${pkgver}.zip::${url}/archive/v${pkgver}.zip"
        "droidcam-modules.conf" "${pkgname}-modprobe.conf"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.desktop"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png")

sha512sums=('937626a4b122739224053b0a2ded9a52b2d7d203614705c06a3205169575f354b88d3721607b347ca9e565689fdd8ff417e26cfde8ed8e69c5f814b0c8a13fc4'
            'aa13c28c7618fa710604c633aa9178cc8a4086abd839915ae579b639405403cf4eb8b692b958032fe43aec07ba8afcb97b998fc9cbdb801004e3d17557eb687e'
            'ca62acc7075e1cd85d32ffd0031fb92cb20c2f9e025011af6b3c380d654268ebdd89d875ebc8d1570c93b78b4242077446e41e2dc033af104409fb5892dac112'
            'd6a273fdf1fd117183b8b97d9415ed28920d106e72eb6155d98615664403ef90f1d0ce0294d482f23737cbde6af450d53166237bbb7ee33da4b40fd3b376f1d5'
            'df0968e8da3b733e29cdda07f0f6f278d4be71d241b21d37631ed8c45b104fea718d05c395b306be8aba2c7acc1a9d69807c4ab9404812b684426ba6d879eea4'
            '8974a76f9d25e114fea8c91e954be3364a1f1d392f034919fc3a63bec46811f8b2bba3fd1eb1ac0de51622c5435a398792e615db23c4c5d9d04fc936aeae8090')

build() {
    cd ${pkgname}-${pkgver}

    # All JPEG* parameters are needed to use shared version of libturbojpeg instead of
    # static one.
    #
    # Also libusbmuxd requires an override while linking.
    make JPEG_DIR="" JPEG_INCLUDE="" JPEG_LIB="" JPEG=$(pkg-config --libs --cflags libturbojpeg) USBMUXD=-lusbmuxd-2.0
}

package() {
    depends=('alsa-lib' "${pkgname}-modules" 'ffmpeg' 'libjpeg-turbo' 'libusbmuxd' 'speex')
    optdepends=('gtk3: use GUI version in addition to CLI interface'
                'libappindicator-gtk3: use GUI version in addition to CLI interface')

    pushd ${pkgname}-${pkgver}

    # Install droidcam program files
    mkdir -p ${pkgdir}/opt/${pkgname}
    install -Dm755 ${pkgname} "${pkgdir}/opt/${pkgname}/${pkgname}"
    install -Dm755 ${pkgname}-cli "${pkgdir}/opt/${pkgname}/${pkgname}-cli"
    install -Dm644 "${srcdir}/${pkgname}-modprobe.conf" "${pkgdir}/etc/modprobe.d/${pkgname}.conf"
    install -Dm644 "${srcdir}/${pkgname}-modules.conf" "${pkgdir}/etc/modules-load.d/${pkgname}.conf"
    install -Dm644 icon2.png "${pkgdir}/opt/${pkgname}-icon.png"

    # Symlink
    mkdir -p ${pkgdir}/usr/bin
    ln -s /opt/${pkgname}/${pkgname} "${pkgdir}/usr/bin/${pkgname}"
    ln -s /opt/${pkgname}/${pkgname}-cli "${pkgdir}/usr/bin/${pkgname}-cli"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}

